package com.zuitt;

import jakarta.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;

	public void init() throws ServletException {
		System.out.println("****************************");
		System.out.println("CalculatorServlet has been initialized... ");
		System.out.println("****************************");
	}

//	 public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{ 
//	}
	 
	public void destroy() {
		System.out.println("****************************");
		System.out.println("CalculatorServlet has been destroyed.");
		System.out.println("****************************");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
	
		String operation = req.getParameter("operation");
		double total= 0;
		PrintWriter out = res.getWriter();
		// try catch - to catch invalid numbers 
		try {
			double num1 = Double.parseDouble(req.getParameter("num1"));
			double num2 = Double.parseDouble(req.getParameter("num2"));
			// I used 'double' so I could also calculate decimal numbers.
			switch (operation) {
			    case "add":
			    case "addition":
			        total = num1 + num2;
			        break;
			    case "subtract":
			    case "subtraction":
			        total = num1 - num2;
			        break;
			    case "multiply":
			    case "multiplication":
			        total = num1 * num2;
			        break;
			    case "divide":
			    case "division":
			    	if (num2 != 0) {
			             total = num1 / num2;
			         } 
			         else {
			             out.println("<p>Error. Division by zero.<p>");
			             return;
			         }
		            break;
		        default:
		        	out.println("<h1>Error: Please provide the correct operation.<h1>");
			        return;
		    }
//			 out.println("<p>The two numbers you provided are : "+(int) num1+" , "+(int) num2+"</p>");
			 out.println("<p>The two numbers you provided are : "+num1+" , "+num2+"</p>");
			 out.println("<p>The operation that you wanted is: " +operation+ "</p>");
			 out.println("<p>The result is: " +total+ "</p>");
        } 
		catch (NumberFormatException e) {
            out.println("<p>Invalid input. Provide the correct number/s.</p>");
            return;
        }
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter(); 
		out.println("<h1> You are now using the calculator app</h1>");
		out.println("<p>To use the app, input the two numbers and an operation. </p>");
		out.println("<p>Hit the submit button after filling in the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");

	}
}
